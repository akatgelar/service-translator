# Sentiment Analysis - Service Translator



## About

This is service for translating SentiWordNet English version to  Indonesia version



## Installation

If you are using windows,

First include the python to venv

```
py -3 -m venv venv
```

And activate the venv

```
venv\Scripts\activate
pip install -r requirement.txt
```

If you are using linux,

First include the python to venv

```
virtualenv -p python3.5 venv --no-site-packages
```

And activate the venv

```
source venv/bin/activate
pip install -r requirement.txt
```



# Usage

First download SentiWordNet 3.0 [here](http://sentiwordnet.isti.cnr.it/), and rename into **SentiWordNet_3.0.0_20130122_English.txt**

Then, create file **SentiWordNet_3.0.0_20130122_Indonesia.txt**

Place both of file on root folder



And then, run the script

```
python translator.py
```



You can see the progress on terminal

See the result data on **SentiWordNet_3.0.0_20130122_Indonesia.txt** file

And activity log on **log/access.log**